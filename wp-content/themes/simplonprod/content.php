<div class="content">
<?php
    $pages = get_pages(array('sort_column' => 'ID', 'sort_order' => 'ASC'));
    foreach( $pages as $page ) :
        $id = $page->ID;
?>

    <article id="<?= $page->post_name; ?>">
        <?php
            wp_reset_query();
            $query = new WP_Query(array('p' => $id, 'post_type' => 'page'));
            while( $query->have_posts() ) :
                $query->the_post();
                the_title();
                the_content();
                the_post_thumbnail();
            endwhile;
        ?>

    </article>
<?php
    endforeach;
?>
</div>
