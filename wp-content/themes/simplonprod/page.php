<?php
/*
Template Name: One slide single page
* Description: A Page Template with an image as background for one page website
*/
*/
?>
<?php get_header(); ?>
<div class="main page">
  <?php if (have_pages()) : ?>
    <?php while (have_pages()) : the_post(); ?>
      <div class="page">
        <h1 class="page-title"><?php the_title(); ?></h1>
        <div class="page-content">
          <p>Nombre de Posts : <strong><?php echo wp_count_pages()->publish; ?></strong></p>
          <p>Nombre de Pages : <strong><?php echo wp_count_pages('page')->publish; ?></strong></p>
          <p>Nombre de commentaires publiés : <strong><?php echo wp_count_comments()->approved; ?></strong></p>
        </div>
      </div>
    <?php endwhile; ?>
  <?php endif; ?>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
