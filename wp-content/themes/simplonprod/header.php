<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 */
?>

<!DOCTYPE html>
<html>
  <head <?php language_attributes(); ?>>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php the_title(); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css" type="text/css">
    <?php wp_head(); ?>
  </head>
  <body>
      <header>
        <h1><a><?php bloginfo('name'); ?></a></h1>
        <h2><?php bloginfo('description'); ?></h2>
      </header>
      <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
#add a static menu !!! http://code.tutsplus.com/tutorials/creating-a-wordpress-theme-from-static-html-adding-navigation--wp-34032

# http://code.tutsplus.com/tutorials/creating-a-wordpress-theme-from-static-html-adding-navigation--wp-34032
