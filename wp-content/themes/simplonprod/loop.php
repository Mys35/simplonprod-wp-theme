#voir la doc http://codex.wordpress.org/The_Loop
#là c'est une boucle pour afficher les pages

<?php
    $pages = get_pages(array(
        'sort_column' => 'menu_order',
        'parent' => 0,
    ));
    $i = 0; foreach($pages as $page): setup_postdata($page);
?>

<div>

    <h1><?php the_title(); ?></h1>
    <?php the_content(); ?>

    <?php
        $children = get_pages(array(
            'sort_column' => 'menu_order',
            'parent' => $page->ID,
        ));
        foreach($children as $child): setup_postdata($child);
    ?>

    <?php endforeach; ?>

</div>

<?php $i++; endforeach; ?>



<?php $args = array(
  'sort_order' => 'ASC',
  'sort_column' => 'ID',
  'post_type' => 'page',
  'post_status' => 'publish'
);
$pages = get_pages($args);
?>
<div class="slide">
    <h1><?php the_title(); ?></h1>
    <?php the_content(); ?>
</div>
